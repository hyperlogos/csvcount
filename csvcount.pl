#!/usr/bin/perl
#
# csvcount.pl - count certain properties of CSV files for analysis
#
# When importing CSV data into a real database you need to know what
# type of data is in the fields, and this script wil tell you.
#
# When provided with a filename it will read a CSV file and tell you
# roughly what kind of data is in the fields, how many lines have
# each type of data. It assumes that the first column has the field
# names and is English-centric. More importantly and eponymously, it
# also tells you the maximum length used for each field.
#
# alpha - contains at least one alpha character
# int - contains only digits
# float - contains only optional leading digit[s], a dot, and more digits
# newline - contains an embedded newline
# empty - empty field

use Text::CSV_XS;
my $csv = Text::CSV_XS->new({binary => 1});

my $file = $ARGV[0] or die "Need to get CSV file on the command line\n";

open my $fh, "<", $file or die "$file: $!";
print "file $file - ";

my $line = 0;
my $errors = 0;

while (my $row = $csv->getline ($fh)) {
    my $column = 0;
    my @fields = @$row;
    my $fieldcount = @fields;
    until ( $column >= $fieldcount ) {
        if ( $line == 0 ) {
            $columns[$column]{'name'} = ( $fields[$column] || '(none)');
	    $columns[$column]{'length'} = 0;
        } else {
            $columns[$column]{'alpha'}++ if $fields[$column] =~ /[A-Za-z]+/;
            $columns[$column]{'int'}++ if $fields[$column] =~ /^[0-9]+$/;
            $columns[$column]{'float'}++ if $fields[$column] =~ /^[0-9]*\.[0-9]+$/;
            $columns[$column]{'newline'}++ if $fields[$column] =~ /\n/;
            $columns[$column]{'length'} = length($fields[$column]) if length($fields[$column]) > $columns[$column]{'length'};
            $columns[$column]{'empty'}++ if (length($fields[$column]) == 0);
        }
        $column++;
    }
    $line++;
}

print "$line lines\n";

foreach my $column ( keys(@columns) ) {
  print "column $column name $columns[$column]{'name'} length $columns[$column]{'length'} ";
  print "alpha $columns[$column]{'alpha'} " if $columns[$column]{'alpha'} > 0;
  print "int $columns[$column]{'int'} " if $columns[$column]{'int'} > 0;
  print "float $columns[$column]{'float'} " if $columns[$column]{'float'} > 0;
  print "newline $columns[$column]{'newline'} " if $columns[$column]{'newline'} > 0;
  print "empty $columns[$column]{'empty'} " if $columns[$column]{'empty'} > 0;
  print "\n";
}

print "\n";
