#!/usr/bin/perl
#
# csvtopg.pl - generate psql to import CSVs into postgres
#
# Does not perform the import; pipe the output to psql or collect it in a
# file in order to actually function

#$settings{'dbschema'} = 'public';    # pg's default schema is 'public'
$settings{'csv_delimiter'} = ',';     # ordinary CSV delimiter is a comma, but varies
$settings{'text_threshold'} = 1;      # over this column width, alpha fields are TEXT instead of VARCHAR(width)
$settings{'drop_table'} = 1;          # set to anything to drop tables before creating, in case you have old ones with different structure

use Cwd qw(getcwd);
my $wd = getcwd;
use Text::CSV_XS;
my $csv = Text::CSV_XS->new({binary => 1});

my $file = $ARGV[0] or die "Need to get CSV file on the command line\n";

open my $fh, "<", $file or die "$file: $!";

my $line = 0;
my $errors = 0;

while (my $row = $csv->getline ($fh)) {
    my $column = 0;
    my @fields = @$row;
    my $fieldcount = @fields;
    until ( $column >= $fieldcount ) {
        if ( $line == 0 ) {
            my $colname = lc($fields[$column]);
            $colname = '' if ( $colname eq "table" );
            $columns[$column]{'name'} = $colname;
            $columns[$column]{'length'} = 0;
        } else {
            $columns[$column]{'alpha'}++ if $fields[$column] =~ /[A-Za-z]+/;
            $columns[$column]{'int'}++ if $fields[$column] =~ /^[0-9]+$/;
            $columns[$column]{'float'}++ if $fields[$column] =~ /^[0-9]*\.[0-9]+$/;
            $columns[$column]{'newline'}++ if $fields[$column] =~ /\n/;
            $columns[$column]{'length'} = length($fields[$column]) if length($fields[$column]) > $columns[$column]{'length'};
            $columns[$column]{'empty'}++ if (length($fields[$column]) == 0);
        }
        $column++;
    }
    $line++;
}

my $unnamed = 1;
my $tablename = $file=~s/\.csv//ir;
my $tablename = lc($tablename);

$tablename = $settings{'dbschema'} . "." . $tablename if $settings{'dbschema'};

print 'DROP TABLE IF EXISTS ' . $tablename . ";\n" if $settings{'drop_table'};

print 'CREATE TABLE IF NOT EXISTS ';
print $tablename . "\(\n";

foreach my $column ( keys(@columns) ) {
    print "  " . ($columns[$column]{'name'}=~s/ /_/gr || 'unnamed' . $unnamed++); # print column name or unnamedN
   
    if ($columns[$column]{'alpha'} > 0) { # column has alpha characters, it is a text field
        if ( ($columns[$column]{'length'} > $settings{'text_threshold'}) || ($columns[$column]{'newline'} > 0) ) {
            print " TEXT"; # use TEXT field if length is over specified threshold length, or field contains embedded breaks
        } else {
            print ' VARCHAR(' . $columns[$column]{'length'} . ')';
        }
    } elsif ( $columns[$column]{'float'} > 0 ) {
        print " REAL";
    } elsif ( $columns[$column]{'int'} > 0 ) {
        print " INTEGER";
    } else {
        print " TEXT"; # if data has to be stuffed into this column later, at least it will fit
    }
    my $tempcol = @columns;
    print "," unless $column >= $tempcol - 1;
    print "\n";
}
print ');' . "\n\n";

print "COPY ";
print $settings{'dbschema'} . "." if $settings{'dbschema'};
print "$tablename\n";
print " FROM '" . $wd . '/' . $file . "'\n";
print " DELIMITER '" . $settings{'csv_delimiter'} . "'\n";
print " NULL ''\n";
print " CSV HEADER;\n\n";
